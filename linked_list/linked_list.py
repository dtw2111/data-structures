# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode:
    def __init__(self, value):
        self.value = value
        self.link = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self._length = 0
    
    @property
    def length(self):
        return self._length

    def get(self, index): # return value of node at given index
        if index < 0:
            index = self._length + index
        node = self.traverse(index)
        return node.value

    def insert(self, value, index=None):
        node = LinkedListNode(value)

        if self.head is None: # insert into empty list
            self.head = node
            self.tail = node
        elif index is not None and index < self._length: # nonempty list, valid index
            if index == 0: # add to front of list
                node.link = self.head # make current head the link of new node
                self.head = node # add new node to head
            else:
                old_node = self.traverse(index - 1) # grab node before where we want to insert
                node.link = old_node.link # make new node's link the same as old node's
                old_node.link = node # reassign old node's link to new node
        else: # when index not < length, add node to link of current tail and set new tail
            self.tail.link = node # make link of current tail new node
            self.tail = node # make tail new node

        self._length += 1
    
    def remove(self, index):
        self._length -= 1 # decrease length before any return statements
        
        if index == 0:
            value = self.head.value
            self.head = self.head.link # make list head the link of old head
            return value # return value of removed original head
        
        before_node = self.traverse(index - 1)
        node_to_remove = before_node.link
        next_node = node_to_remove.link

        value = node_to_remove.value
        before_node.link = next_node # connect two nodes on either side of node_to_remove

        if next_node is None: # deleting last item in list
            self.tail = before_node # make new tail node before removed node

        
        return value

    def traverse(self, index): # returns node at given index of list
        if index >= self._length:
            raise IndexError("index out of range")
        node = self.head
        i = 0
        while i < index:
            node = node.link
            i += 1
        return node

