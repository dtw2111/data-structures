# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# circular_queue.
# 
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_XX.py.

class CircularQueue:
    def __init__(self, size):
        self.size = size
        self.length = 0
        self.head = 0
        self.tail = 0
        self.buffer = [None] * size # empty placeholder list of correct size
    
    def enqueue(self, value):
        if self.length == self.size:
            raise IndexError("queue is full")
        self.length += 1
        self.buffer[self.tail] = value
        self.tail += 1
        if self.tail >= self.size:
            self.tail = 0 # if tail passes end of buffer, new tail is beginning of buffer
        return value
    
    def dequeue(self):
        if self.length == 0:
            raise IndexError("queue is already empty")
        self.length -= 1
        value = self.buffer[self.head]
        self.head += 1
        if self.head >= self.size:
            self.head = 0 # handle looping head back around to buffer[0]
        return value


